import gazu
import qarnot_auto_render
import os

os.environ['BLENDER_APP'] = '/Applications/Blender.app/Contents/MacOS/blender'
os.environ['TMP_DIR'] = '/Users/felixdavid/Documents/ATI/Intensif/M2/Pipeline/TMP'

gazu.set_event_host("https://kitsu.felixdavid.com/socket.io")

def my_callback(data):
    qarnot_auto_render.main(data)

event_client = gazu.events.init()
gazu.events.add_listener(event_client, "task:update", my_callback)
gazu.events.run_client(event_client)
bl_info = {
    "name": "Gobelin Version Publishing",
    "author": "Félix David",
    "version": (0, 3, 4),
    "blender": (2, 81, 1),
    "location": "File > Export > Publish Version",
    "description": "Publish versions automatically in Kitsu and copies to Lise's server",
    "warning": "",
    "category": "Pipeline tool",
}

import bpy
import tempfile
import json
from pathlib import Path
from ftplib import FTP
import os
import shutil
import sys

# Install Gazu module into Blender
def install_lib(libname):
    from subprocess import call

    pp = bpy.app.binary_path_python
    call([pp, "-m", "ensurepip", "--user"])
    call([pp, "-m", "pip", "install", "--user", libname])

try:
    import gazu

except ModuleNotFoundError:
    install_lib("gazu")
    import gazu

try:
    import qarnot

except ModuleNotFoundError:
    install_lib("qarnot")
    import qarnot

# Element types loading, very very dirty
gazu.set_host('https://kitsu.felixdavid.com/api')
gazu.log_in('stax@stax.com', 'stax_password')
asset_types = [(t.get('name'), t.get('name'), t.get('name'))
               for t in gazu.asset.all_asset_types()]

def menu_func_export(self, context):
    self.layout.operator(PublishVersion.bl_idname, text="Publish Version")


def element_types_callback(self, context):
    return asset_types

## FTP
# Change directories - create if it doesn't exist
def chdir(ftp, dir):
    mkdir(ftp, dir)
    ftp.cwd(dir)

# Create directories
def mkdir(ftp, dir):
    if not directory_exists(ftp, dir):
        ftp.mkd(dir)

# Check if directory exists (in current location)
def directory_exists(ftp, dir):
    filelist = []
    ftp.retrlines('LIST',filelist.append)
    for f in filelist:
        if f.split()[-1] == dir and f.upper().startswith('D'):
            return True
    return False

class PublishVersion(bpy.types.Operator):
    """Publish current version"""
    bl_idname = "wm.publish_version"
    bl_label = "Publish Version"

    kitsu_host: bpy.props.StringProperty(name='Kitsu Host', default='https://kitsu.felixdavid.com/api',
                                         options={'HIDDEN'})
    user_login: bpy.props.StringProperty(name='Log-in', default='')
    user_password: bpy.props.StringProperty(name='Password', default='', subtype='PASSWORD')
    revision_comment: bpy.props.StringProperty(name='Revision comment', default='')
    task_status: bpy.props.EnumProperty(name='Task status', items=[
        ('Work In Progress', 'Work In Progress', 'Work In Progress'),
        ('Waiting For Approval', 'Waiting For Approval', 'Waiting For Approval'),
        ('Done', 'Done', 'Done')])
    render: bpy.props.BoolProperty(name="Render shot on Qarnot", default=True)
    make_final: bpy.props.BoolProperty(name="Final version", default=False)
    publish_textures: bpy.props.BoolProperty(name="Publish 'Textures' directory.", default=False)

    @classmethod
    def poll(cls, context):
        return context.scene.camera is not None

    def invoke(self, context, event):
        # Credentials
        f = Path(tempfile.gettempdir(), 'gobpass')
        if f.is_file():
            with f.open() as config_stream:
                f = json.load(config_stream)
        else:
            f = {}
        self.user_login = f.get('login', '')
        self.user_password = f.get('pass', '')

        return context.window_manager.invoke_props_dialog(self)

    def draw(self, context):
        layout = self.layout

        col = layout.column()
        col.prop(context.scene, 'local_use')
        col.prop(self, 'user_login')
        col.prop(self, 'user_password')
        col.prop(context.scene, 'element_category')
        if context.scene.element_category == 'Assets':
            col.prop(context.scene, 'element_type')
            col.prop(self, 'publish_textures')
        else:
            col.prop(self, 'render')
        col.prop(self, 'task_status', text='Status')
        col.prop(self, 'revision_comment', text='Comment')
        col.prop(self, 'make_final')

    def execute(self, context):
        # Display loading for user
        context.window.cursor_set("WAIT")

        scene = bpy.context.scene

        # Credentials
        cred = {'login': self.user_login, 'pass': self.user_password}
        with open(Path(tempfile.gettempdir(), 'gobpass'), 'w') as f:
            json.dump(cred, f)

        default_settings = [context.scene.render.filepath,
                            context.scene.render.image_settings.file_format]

        # Check if production tree is conform to nomenclature
        is_tree_conform = False
        production_dir_source = Path()
        for dir in Path(bpy.data.filepath).resolve().parts:
            production_dir_source = production_dir_source.joinpath(dir)
            if dir == 'Production':
                is_tree_conform = True
                break
        if not is_tree_conform:
            self.report({'ERROR'}, "Production tree not conform to nomenclature. 'Production' directory is missing")
            return {'FINISHED'}
        production_dir_source = production_dir_source.resolve()

        # Get filename
        blend_filename = bpy.path.basename(bpy.data.filepath)
        blend_filename = blend_filename.split('.')[0]  # Get only the name

        # Check file
        if context.scene.element_category == 'Assets':
            try:
                element_name, element_task, element_version = blend_filename.split('_')
                if not self.check_nomenclature([element_name, element_task]):
                    return {'FINISHED'}

            except ValueError as e:
                print('ERROR:', e)
                self.report({'ERROR'}, ' '.join(['Wrong file name:', blend_filename,
                                                 'Please fit the expected pattern: Name_Task_##.blend']))
                return {'FINISHED'}
        else:
            try:
                element_name, element_task, element_version = blend_filename.split('_')
                sequence_element_name = element_name
                sequence_name, element_name = element_name.split('-')

                if not self.check_nomenclature([element_name, element_task, sequence_name]):
                    return {'FINISHED'}

            except ValueError as e:
                print('ERROR:', e)
                self.report({'ERROR'}, ' '.join(['Wrong file name:', blend_filename,
                                                 'Please fit the expected pattern: SeqName-ShotName_Task_##.blend']))
                return {'FINISHED'}

        # Copy blend file to server
        if context.scene.element_category == 'Assets':
            element_path = Path(context.scene.element_category,
                                element_name,
                                element_task,
                                '_'.join([element_name, element_task, element_version])).with_suffix('.blend')
        else:
            element_path = Path(context.scene.element_category,
                                sequence_name,
                                element_name,
                                element_task,
                                '_'.join([sequence_element_name, element_task, element_version])).with_suffix('.blend')
            # Set render name
            context.scene.render.filepath = '_'.join([sequence_element_name, element_task, '####'])

        bpy.ops.file.make_paths_relative()
        bpy.ops.wm.save_mainfile()

        print('Uploading blend file ...')
        if not self.send_to_server(bpy.data.filepath, element_path):
            return {'FINISHED'}

        # Make final
        if self.make_final:
            print('Uploading final blend file ...')
            # Element folder
            if context.scene.element_category == 'Assets':
                final_path = element_path.with_name(
                    '_'.join([element_name, element_task, 'Final'])).with_suffix('.blend')
            else:
                final_path = element_path.with_name(
                    '_'.join([sequence_element_name, element_task, 'Final'])).with_suffix('.blend')
            if not self.send_to_server(bpy.data.filepath, final_path):
                return {'FINISHED'}

            # Final folder
            final_path = Path('Final').joinpath(final_path)
            if not self.send_to_server(bpy.data.filepath, final_path):
                return {'FINISHED'}

        # Copy production folders to server
        print('Uploading production folders...')
        if self.publish_textures:
            production_folders = ['Textures', 'References']
            for folder in production_folders:
                path = Path(bpy.data.filepath).parent.joinpath(folder)
                if path.is_dir():
                    for (dir_path, dir_names, files) in os.walk(path):
                        for file in files:
                            if not file.startswith('.'):
                                child = Path(dir_path).joinpath(file)
                                if not self.send_to_server(child,
                                                           element_path.parent.joinpath(child.relative_to(path.parent))):
                                    return {'FINISHED'}

                                if self.make_final and folder != 'References':
                                    if not self.send_to_server(child,
                                                               final_path.parent.joinpath(
                                                                   child.relative_to(path.parent))):
                                        return {'FINISHED'}

        # Check Production tracker
        print("Updating Kitsu...")
        # Check host
        gazu.set_host(self.kitsu_host)
        gazu.log_in(self.user_login, self.user_password)

        project = gazu.project.get_project_by_name('Gobelin')
        elements = gazu.asset.all_assets_for_project(project) if context.scene.element_category == 'Assets' \
            else gazu.shot.all_shots_for_project(project)

        # Get element
        if context.scene.element_category == 'Shots':
            current_sequence = gazu.shot.get_sequence_by_name(project, sequence_name)
            if not current_sequence:
                current_sequence = gazu.shot.new_sequence(project, sequence_name)


        current_element = gazu.asset.get_asset_by_name(project, element_name) if context.scene.element_category == 'Assets' \
            else gazu.shot.get_shot_by_name(current_sequence, element_name)
        if not current_element in elements:
            if context.scene.element_category == 'Assets':
                asset_type = gazu.asset.get_asset_type_by_name(context.scene.element_type)
                current_element = gazu.asset.new_asset(project, asset_type, element_name)
            else:
                current_element = gazu.shot.new_shot(project, current_sequence, element_name)

        # Update task
        task_type = gazu.task.get_task_type_by_name(element_task)
        all_tasks = gazu.task.all_task_types_for_asset(current_element) if context.scene.element_category == 'Assets' \
            else gazu.task.all_task_types_for_shot(current_element)
        if not task_type in all_tasks:
            current_element_task = gazu.task.new_task(current_element, task_type)
        else:
            current_element_task = gazu.task.get_task_by_name(current_element, task_type)

        # Publish revision
        # Update element data
        print("Updating Kitsu -> Element data...")

        if not current_element.get('data'):
            current_element['data'] = {}
        current_element['data']['current_version'] = element_version

        if context.scene.element_category == 'Assets':
            gazu.asset.update_asset(current_element)
        else:
            current_element['data']['render'] = False
            gazu.shot.update_shot(current_element)

        # Update task
        print("Updating Kitsu -> Task...")

        status = gazu.task.get_task_status_by_name(self.task_status)
        comment = gazu.task.add_comment(current_element_task, status, self.revision_comment)

        if context.scene.element_category == 'Assets':
            # Render preview for asset
            context.scene.render.image_settings.file_format = 'JPEG'

            # Render for each camera
            preview_file = None
            i = 0
            for obj in context.scene.objects:
                if obj.type == 'CAMERA' and not obj.hide_render:
                    blend_file_path = Path(bpy.data.filepath).resolve()
                    preview_path = Path(blend_file_path.parent, 'Previews', '-'.join([blend_filename, str(i).zfill(2)]))\
                        .with_suffix('.' + context.scene.render.image_settings.file_format.lower()).resolve()
                    context.scene.render.filepath = str(preview_path)

                    context.scene.camera = obj
                    bpy.ops.render.render(write_still=True)

                    # Update Kitsu
                    preview_file = gazu.task.add_preview(
                        current_element_task,
                        comment,
                        context.scene.render.filepath
                    )

                    preview_server_path = element_path.parent.joinpath('Previews', preview_path.name)

                    if not self.send_to_server(context.scene.render.filepath, preview_server_path):
                        return {'FINISHED'}

                    i += 1

            gazu.task.set_main_preview(current_element, preview_file)

        # Set settings back
        context.scene.render.filepath, context.scene.render.image_settings.file_format = default_settings

        # Render on Qarnot
        if scene.element_category == 'Shots' and self.render:
            print("Render on Qarnot...")

            temp_folder = Path(tempfile.gettempdir())
            temp_production_folder = temp_folder.joinpath('Production')

            # Build folder to be sent to Qarnot
            # Copy current_blend_file
            temp_blend_file = temp_production_folder.joinpath(element_path)
            if not temp_blend_file.parent.is_dir():
                temp_blend_file.parent.mkdir(parents=True)

            shutil.copy(bpy.data.filepath, temp_blend_file)

            # Copy dependencies
            # Libraries
            for lib in bpy.data.libraries.values():
                source_path = Path(bpy.data.filepath).parent.joinpath(lib.filepath[2:]).resolve()

                if not source_path.is_file():
                    self.report({'ERROR'}, ' '.join(['Cannot find dependency blend file:', str(source_path)]))
                    return {'FINISHED'}

                target_path = temp_production_folder.joinpath(source_path.relative_to(production_dir_source))

                if not target_path.parent.is_dir():
                    target_path.parent.mkdir(parents=True)

                shutil.copy(source_path, target_path)

            # Images
            for img in bpy.data.images.values():
                if img.type == 'IMAGE':
                    source_path = Path(img.filepath_from_user()).resolve()

                    if not source_path.is_file():
                        self.report({'ERROR'}, ' '.join(['Cannot find image file:', str(source_path)]))
                        return {'FINISHED'}

                    target_path = temp_production_folder.joinpath(source_path.relative_to(production_dir_source))

                    if not target_path.parent.is_dir():
                        target_path.parent.mkdir(parents=True)

                    shutil.copy(source_path, target_path)

            conn = qarnot.Connection(cluster_url='https://api.qarnot.com/',
                                     client_token='37c81b3380df74540f575768a3c63b6fcc1576148103efa7a0b6ff611bd0d395')

            # Create a task.
            # Because we are selecting the frame to render inside the task, put the frame count to 0
            task = conn.create_task(blend_filename, 'blender-2.81a', 0)

            # Store if an error happened during the process
            error_happened = False
            try:
                # Create a resource bucket and add an input file
                print("** Uploading %s and all its dependencies..." % str(blend_filename))
                input_bucket = conn.retrieve_bucket('gobelin-production')
                input_bucket.add_directory(str(temp_production_folder))

                # Attach the bucket to the task
                task.resources.append(input_bucket)

                # Create and attach an output bucket
                output_bucket = conn.create_bucket(blend_filename + '-render')
                task.results = output_bucket

                # Render the frame defined by the scene
                task.advanced_range = '-'.join([str(scene.frame_start),
                                                str(scene.frame_end)])

                # Task constants are the main way of controlling a task's behaviour
                task.constants['BLEND_FILE'] = str(element_path).replace('\\', '/')
                task.constants['BLEND_SLICING'] = '1'
                task.constants['BLEND_ENGINE'] = 'CYCLES'
                task.constants['BLEND_CYCLES_SAMPLES'] = str(scene.cycles.samples)
                if scene.render.image_settings.file_format in ['OPEN_EXR_MULTILAYER', 'OPEN_EXR']:
                    task.constants['BLEND_FORMAT'] = scene.render.image_settings.file_format.split('_')[-1]
                else:
                    task.constants['BLEND_FORMAT'] = scene.render.image_settings.file_format
                task.constants['BLEND_W'] = str(scene.render.resolution_x)
                task.constants['BLEND_H'] = str(scene.render.resolution_y)
                task.constants['BLEND_RATIO'] = str(scene.render.resolution_percentage)

                # Submit the task to the Api, that will launch it on the cluster
                task.submit()

                render_folder = temp_folder.joinpath('Render')
                rendered_frames_dir = render_folder.joinpath(blend_filename + '-frames')
                copied_frames = []

                # Wait for the task to be finished, and monitor its progress
                last_state = ''
                last_execution_progress = 0.0
                done = False
                print("Download rendered frames every minute...")
                while not done:
                    if task.status is not None and task.status.execution_progress != last_execution_progress:
                        last_execution_progress = task.status.execution_progress
                        print("** Overall progress {}%".format(last_execution_progress))

                    if task.state != last_state:
                        last_state = task.state
                        print("** {}".format(last_state))

                    # Wait for the task to complete, with a timeout of 5 seconds.
                    # This will return True as soon as the task is complete, or False
                    # after the timeout.
                    done = task.wait(5)

                    # Display fresh stdout / stderr
                    sys.stdout.write(task.fresh_stdout())
                    sys.stderr.write(task.fresh_stderr())

                    # Download succeeded frames
                    task.snapshot(60)
                    task.download_results(str(rendered_frames_dir))

                    # Copy rendered frames to server
                    for img in rendered_frames_dir.iterdir():
                        if img not in copied_frames:
                            frames_dir = element_path.parent.joinpath('Frames')
                            if img.is_file():
                                print("Copy last rendered frame to server...")
                                sent = self.send_to_server(img, frames_dir.joinpath(img.name))
                                if sent:
                                    copied_frames.append(img)


                # Display errors on failure
                if task.state == 'Failure':
                    print("** Errors: %s" % task.errors[0])
                    error_happened = True


            finally:
                # task.delete()
                # Exit code in case of error
                if error_happened:
                    self.report({'ERROR'}, 'Error happend with Qarnot: ' + str(task.errors[0]))
                    return {'FINISHED'}

        # Save as and increment
        print("Save as and increment...")

        if context.scene.element_category == 'Assets':
            filepath = Path(Path(bpy.data.filepath).parent, '_'.join([element_name, element_task,
                                                                          str(int(element_version) + 1).zfill(
                                                                              2)])).with_suffix('.blend')
        else:
            filepath = Path(Path(bpy.data.filepath).parent, '_'.join([sequence_element_name, element_task,
                                                                          str(int(element_version) + 1).zfill(
                                                                              2)])).with_suffix('.blend')

        if self.make_final:
            bpy.ops.wm.save_as_mainfile(filepath=str(filepath.with_name(final_path.name)))

        bpy.ops.wm.save_as_mainfile(filepath=str(filepath))

        self.report({'INFO'}, context.scene.element_category + ' version published!')

        return {'FINISHED'}

    def check_nomenclature(self, words):
        for word in words:
            if word[0] != word[0].upper():
                self.report({'ERROR'}, ' '.join(['Wrong file naming.', 'In the filename, the word:', '"', word, '"',
                                                 'must start with a capital letter.']))
                return False
        return True

    def send_to_server(self, source_path, target_path):
        if bpy.context.scene.local_use:
            if os.environ['GOB_DIR']:
                # Copy local
                element_path = Path(os.environ['GOB_DIR']).joinpath(target_path).resolve()
                if not element_path.parent.is_dir():
                    element_path.parent.mkdir(parents=True)
                shutil.copy2(source_path, element_path)
            else:
                self.report({'ERROR'},
                            'Please create a "GOB_DIR" environment variable with the "Production" folder path.')
                return False
        else:
            # Copy via FTP
            ftp = FTP('zenith.sytes.net')
            ftp.login('ati_intensifs', 'm=X&ts5d')
            ftp.cwd('Intensifs/M2_Gobelin/Production')

            file = open(source_path, 'rb')

            for dir in target_path.parts[:-1]:
                chdir(ftp, str(dir))

            ftp.storbinary('STOR ' + str(target_path.name), file)
            file.close()
            ftp.quit()

        return True


def register():
    bpy.utils.register_class(PublishVersion)
    bpy.types.TOPBAR_MT_file_export.append(menu_func_export)

    bpy.types.Scene.element_category = bpy.props.EnumProperty(name='Category', items=[('Assets', 'Asset', 'Asset'),
                                                                     ('Shots', 'Shot', 'Shot')])
    bpy.types.Scene.element_type = bpy.props.EnumProperty(name='Type', items=element_types_callback)
    bpy.types.Scene.local_use = bpy.props.BoolProperty(name="Local use (if you are in Lise's apartment)", default=True)


def unregister():
    bpy.utils.unregister_class(PublishVersion)
    bpy.types.TOPBAR_MT_file_export.remove(menu_func_export)

    del(bpy.types.Scene.element_category)
    del(bpy.types.Scene.element_type)
    del(bpy.types.Scene.local_use)


if __name__ == "__main__":
    register()

import qarnot
import gazu
import ftplib
from pathlib import Path
import os
import platform
import subprocess
import json
import sys


def ftp_download(path):
    pass


def main(task_data, dev=False, render=True):
    # Kitsu Connect
    gazu.set_host('https://kitsu.felixdavid.com/api')
    gazu.log_in('stax@stax.com', 'stax_password')

    current_task = gazu.task.get_task(task_data.get('task_id'))
    current_shot = current_task.get('entity')

    # Quit if not shot
    if current_task.get('entity_type').get('name') != 'Shot' or not current_shot.get('data', {}).get('render', False):
        return False

    # Get shot informations
    shot_sequence = current_task.get('sequence').get('name')
    shot_name = current_shot.get('name')
    shot_full_name = '-'.join([shot_sequence, shot_name])
    shot_task = current_task.get('task_type').get('name')
    shot_version = str(int(current_shot.get('data').get('current_version'))).zfill(2)

    # Build blend file name and file path on server
    blend_file_name = Path('_'.join([shot_full_name, shot_task, shot_version])).with_suffix('.blend')
    blend_filepath_server = Path('Shots', shot_sequence, shot_name, shot_task, blend_file_name)

    # Get blend from ftp
    ftp = ftplib.FTP('zenith.sytes.net')
    ftp.login('ati_intensifs', 'm=X&ts5d')
    ftp.cwd('Intensifs/M2_Gobelin/Production/')

    temp_folder = Path(os.environ.get('TMP_DIR'), 'TMP').resolve()
    production_folder = temp_folder.joinpath('Production')
    input_file, blend_dep = download_blend_tree_ftp(ftp, blend_filepath_server, production_folder, dev)
    ftp.quit()

    # Upload to Qarnot
    conn = qarnot.Connection(str(Path(__file__).parent.joinpath('qarnot.conf').resolve()))

    # Create a task.
    # Because we are selecting the frame to render inside the task, put the frame count to 0
    task = conn.create_task(blend_file_name.stem, 'blender-2.81a', 0)

    # Store if an error happened during the process
    error_happened = False
    try:
        if render:
            # Create a resource bucket and add an input file
            print("** Uploading %s..." % str(input_file))
            input_bucket = conn.retrieve_bucket('gobelin-production')
            input_bucket.add_directory(str(production_folder))

            # Attach the bucket to the task
            task.resources.append(input_bucket)

            # Create and attach an output bucket
            output_bucket = conn.create_bucket(blend_file_name.stem + '-render')
            task.results = output_bucket

            # Render the frame 115 to 120
            task.advanced_range = '-'.join([str(blend_dep.get('frame_start')), str(blend_dep.get('frame_end'))])

            # Task constants are the main way of controlling a task's behaviour
            task.constants['BLEND_FILE'] = str(blend_filepath_server)
            task.constants['BLEND_SLICING'] = '1'
            task.constants['BLEND_ENGINE'] = 'CYCLES'
            task.constants['BLEND_FORMAT'] = 'PNG'
            task.constants['BLEND_W'] = '1920'
            task.constants['BLEND_H'] = '1080'
            task.constants['BLEND_RATIO'] = '50'  # Limit ratio for testing purposes
            task.constants['BLEND_CYCLES_SAMPLES'] = '20'

            # Submit the task to the Api, that will launch it on the cluster
            task.submit()

            # Wait for the task to be finished, and monitor its progress
            last_state = ''
            last_execution_progress = 0.0
            done = False
            while not done:
                if task.status is not None and task.status.execution_progress != last_execution_progress:
                    last_execution_progress = task.status.execution_progress
                    print("** Overall progress {}%".format(last_execution_progress))

                if task.state != last_state:
                    last_state = task.state
                    print("** {}".format(last_state))

                # Wait for the task to complete, with a timeout of 5 seconds.
                # This will return True as soon as the task is complete, or False
                # after the timeout.
                done = task.wait(5)

                # Display fresh stdout / stderr
                sys.stdout.write(task.fresh_stdout())
                sys.stderr.write(task.fresh_stderr())

        # Display errors on failure
        if task.state == 'Failure':
            print("** Errors: %s" % task.errors[0])
            error_happened = True

        # Download succeeded frames
        print("Download rendered frames...")
        render_folder = temp_folder.joinpath('Render')
        rendered_frames_dir = render_folder.joinpath(blend_file_name.stem + '-frames')
        task.download_results(str(rendered_frames_dir))

        # Copy rendered frames to ftp
        print("Copy rendered frames to server...")
        for img in rendered_frames_dir.iterdir():
            frames_dir = blend_filepath_server.parent.joinpath('Frames')
            if img.is_file():
                send_to_server(img, frames_dir.joinpath(img.name))

    finally:
        task.delete()
        # Exit code in case of error
        if error_happened:
            sys.exit(1)

## FTP
# Change directories - create if it doesn't exist
def chdir(ftp, dir):
    mkdir(ftp, dir)
    ftp.cwd(dir)

# Create directories
def mkdir(ftp, dir):
    if not directory_exists(ftp, dir):
        ftp.mkd(dir)


# Check if directory exists (in current location)
def directory_exists(ftp, dir):
    filelist = []
    ftp.retrlines('LIST', filelist.append)
    for f in filelist:
        if f.split()[-1] == dir and f.upper().startswith('D'):
            return True
    return False

def send_to_server(source_path, target_path):
    # Copy via FTP
    ftp = ftplib.FTP('zenith.sytes.net')
    ftp.login('ati_intensifs', 'm=X&ts5d')
    ftp.cwd('Intensifs/M2_Gobelin/Production')

    file = open(source_path, 'rb')

    for dir in target_path.parts[:-1]:
        chdir(ftp, str(dir))

    ftp.storbinary('STOR ' + str(target_path.name), file)
    file.close()
    ftp.quit()

    return True


def download_blend_tree_ftp(ftp, blend_filepath_server, temp_folder, dev=False):
    """Download given blend file and all its dependencies. For now, it's done according to the Gobelin project
    nomenclature.
    Return master Blender file path.
    """
    current_blend_file = temp_folder.joinpath(blend_filepath_server).resolve()
    if not current_blend_file.parent.is_dir():
        current_blend_file.parent.mkdir(parents=True)
    if not dev:
        ftp.retrbinary("RETR " + str(blend_filepath_server), open(str(current_blend_file), "wb").write)

    # Get dependencies from file
    dependencies_buffer_file = Path('dependencies.json').resolve()
    dependencies = get_blend_dependencies(current_blend_file, dependencies_buffer_file)
    print(dependencies)

    # Download dependencies locally
    if not dev:
        # Libraries
        new_dependencies_paths_local = {}
        new_dependencies_paths_local['libraries'] = {}
        for lib, filepath in dependencies.get('libraries', {}).items():
            filename = Path(filepath)
            asset_name, asset_task, asset_version = filename.stem.split('_')
            filepath_server = Path('Assets', asset_name, asset_task, filename.name)
            filepath_local = temp_folder.joinpath(filepath_server).resolve()
            if not filepath_local.parent.is_dir():
                filepath_local.parent.mkdir(parents=True)

            download_blend_tree_ftp(ftp, filepath_server, temp_folder, dev)
            new_dependencies_paths_local['libraries'][lib] = str(filepath_local)

        # Images
        new_dependencies_paths_local['images'] = {}
        for img, filepath in dependencies.get('images', {}).items():
            filepath_server = blend_filepath_server.parent.joinpath(filepath[2:])
            filepath_local = temp_folder.joinpath(filepath_server).resolve()
            if not filepath_local.parent.is_dir():
                filepath_local.parent.mkdir(parents=True)
            print(filepath)
            ftp.retrbinary("RETR " + str(filepath_server), open(str(filepath_local), "wb").write)

            new_dependencies_paths_local['images'][img] = str(filepath_local)

        update_blend_dependencies_paths(current_blend_file, new_dependencies_paths_local)

    return current_blend_file, dependencies


def update_blend_dependencies_paths(blend_file, new_dependencies_paths):
    """Updates blend file dependencies filepaths to given ones. Images and libraries.
    """
    python_script_path = Path(__file__).parent.joinpath('blender_update_file_dependencies.py').resolve()
    if platform.system() != 'Windows':
        # Set alias to blender in you system
        cmd = [os.environ.get('BLENDER_APP'), '-b', str(blend_file), '--python', str(python_script_path),
               '--', str(new_dependencies_paths)]
        subprocess.call(cmd)
    else:
        print('Make it work for windows')


def get_blend_dependencies(blend_file, dependencies_buffer_file):
    """Get blend dependencies, images and libraries."""
    python_script_path = Path(__file__).parent.joinpath('blender_get_file_dependencies.py').resolve()
    if platform.system() != 'Windows':
        # Set alias to blender in you system
        print(dependencies_buffer_file)
        cmd = [os.environ.get('BLENDER_APP'), '-b', str(blend_file), '--python', str(python_script_path),
               '--', str(dependencies_buffer_file)]
        subprocess.call(cmd)

        with dependencies_buffer_file.open('r') as f:
            return json.load(f)
    else:
        print('Make it work for windows')

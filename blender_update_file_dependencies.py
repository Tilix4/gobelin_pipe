import bpy
import sys
import ast

# Get arguments
argv = sys.argv
argv = argv[argv.index("--") + 1:]
dependencies_filepaths = ast.literal_eval(argv[0])
print(dependencies_filepaths)

# Update dependencies
for key, value in dependencies_filepaths.items():
    for dep_name, filepath in value.items():
        if key == 'libraries':
            dep = bpy.data.libraries.get(dep_name)
        elif key == 'images':
            dep = bpy.data.images.get(dep_name)
        else:
            dep = None

        if dep:
            dep.filepath = filepath


bpy.ops.file.make_paths_relative()
bpy.ops.wm.save_mainfile()
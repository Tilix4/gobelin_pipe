import bpy
import json
import sys

# Get arguments
argv = sys.argv
argv = argv[argv.index("--") + 1:]
dependencies_filepath = argv[0]

# Get dependencies
dependencies = {}
dependencies['libraries'] = {}
for key, value in bpy.data.libraries.items():
    dependencies['libraries'][key] = value.filepath

dependencies['images'] = {}
for key, value in bpy.data.images.items():
    if value.type != 'RENDER_RESULT' and value.filepath:
        dependencies['images'][key] = value.filepath

# Add other needed informations
dependencies['frame_start'] = bpy.context.scene.frame_start
dependencies['frame_end'] = bpy.context.scene.frame_end

# Write them to given file
with open(dependencies_filepath, 'w') as f:
    json.dump(dependencies, f)
